
from ._version import get_versions
__version__ = get_versions()['version'][1:]
del get_versions
